package eu.paliga.volanto;

/**
 * Created by krzys on 07.09.2016.
 */
public class Api {

    public static String BASE_URL = "http://recruitment.volanto.pl:23890/";

    public static String FB_LOGIN = BASE_URL + "tokens/facebook";
    public static String LOGIN = BASE_URL + "tokens";
    public static String CONTACTS = BASE_URL + "contacts";

    public static class Fields {
        public static String token = "value";
        public static String fbToken = "accessToken";
        public static String email = "email";
        public static String password = "password";
        public static String firstName = "firstName";
        public static String lastName = "lastName";
        public static String companyName = "companyName";
        public static String phoneNumber = "phoneNumber";
        public static String id = "id";
        public static String content = "content";
    }

    public static class Headers {
        public static String auth = "X-AUTH-TOKEN";
        public static String content_type = "Content-Type";
        public static String application_json = "application/json";
    }
}
