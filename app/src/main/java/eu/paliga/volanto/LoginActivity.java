package eu.paliga.volanto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.login)
    Button login;

    RequestQueue requestQueue;

//    @Bind(R.id.login_button)
//    LoginButton loginButton;
//
//    private CallbackManager callbackManager;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
//        AppEventsLogger.activateApp(this);

        requestQueue = Volley.newRequestQueue(this);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        loginButton.setReadPermissions("email");
//
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @DebugLog
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                String fb_token = loginResult.getAccessToken().getToken();
//                volantoFBLogin(fb_token);
//            }
//
//            @DebugLog
//            @Override
//            public void onCancel() {
//
//            }
//
//            @DebugLog
//            @Override
//            public void onError(FacebookException error) {
//                Toast.makeText(getBaseContext(), R.string.facebook_login_failed, Toast.LENGTH_SHORT).show();
//            }
//        });

        login.setOnClickListener(new View.OnClickListener() {
            @DebugLog
            @Override
            public void onClick(View view) {
                String email = LoginActivity.this.email.getText().toString();
                String password = LoginActivity.this.password.getText().toString();

                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(getBaseContext(), R.string.email_or_password_empty, Toast.LENGTH_SHORT).show();
                } else {
                    volantoLogin(email, password);
                }
            }
        });
    }

    @DebugLog
    private void volantoFBLogin(String fb_token) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.Fields.fbToken, fb_token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, Api.FB_LOGIN, jsonObject,
                new Response.Listener<JSONObject>() {
                    @DebugLog
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString(Api.Fields.token);
                            SharedPrefs.saveToken(getBaseContext(), token);
                            Intent intent = new Intent(getBaseContext(), ListActivity.class);
                            startActivity(intent);
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), R.string.crm_login_failed, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), R.string.crm_login_failed, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }

    @DebugLog
    private void volantoLogin(String email, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.Fields.email, email);
            jsonObject.put(Api.Fields.password, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, Api.LOGIN, jsonObject,
                new Response.Listener<JSONObject>() {
                    @DebugLog
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString(Api.Fields.token);
                            SharedPrefs.saveToken(getBaseContext(), token);
                            Intent intent = new Intent(getBaseContext(), ListActivity.class);
                            startActivity(intent);
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), R.string.crm_login_failed, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), R.string.crm_login_failed, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        requestQueue.add(jsonObjectRequest);
    }
}
