package eu.paliga.volanto;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

/**
 * Created by krzys on 07.09.2016.
 */
public class ListActivity extends AppCompatActivity {

    @Bind(R.id.contacts)
    RecyclerView contacts;
    ContactAdapter contactsAdapter;

    @Bind(R.id.add)
    Button add;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);

        requestQueue = Volley.newRequestQueue(getBaseContext());

        fetchContacts();

        ButterKnife.bind(this);

        contactsAdapter = new ContactAdapter();
        contacts.setAdapter(contactsAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        contacts.setLayoutManager(llm);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), ContactActivity.class);
                startActivityForResult(intent, 0);
            }
        });
    }

    @DebugLog
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        fetchContacts();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.id)
        TextView id;
        @Bind(R.id.firstName)
        TextView firstName;
        @Bind(R.id.lastName)
        TextView lastName;
        @Bind(R.id.companyName)
        TextView companyName;
        @Bind(R.id.email)
        TextView email;
        @Bind(R.id.phoneNumber)
        TextView phoneNumber;
        @Bind(R.id.image)
        ImageView imageView;

        public ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getBaseContext(), ContactActivity.class);
                    intent.putExtra(Api.Fields.id, id.getText().toString());
                    intent.putExtra(Api.Fields.firstName, firstName.getText().toString());
                    intent.putExtra(Api.Fields.lastName, lastName.getText().toString());
                    intent.putExtra(Api.Fields.companyName, companyName.getText().toString());
                    intent.putExtra(Api.Fields.email, email.getText().toString());
                    intent.putExtra(Api.Fields.phoneNumber, phoneNumber.getText().toString());

                    startActivityForResult(intent, 0);
                }
            });
        }
    }

    private class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {
        JSONArray contacts = new JSONArray();

        @Override
        public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_contact, parent, false);

            return new ContactViewHolder(itemView);
        }

        @DebugLog
        @Override
        public void onBindViewHolder(ContactViewHolder holder, int position) {
            try {
                JSONObject contact = contacts.getJSONObject(position);
                holder.id.setText(contact.getString(Api.Fields.id));
                holder.firstName.setText(contact.getString(Api.Fields.firstName));
                holder.lastName.setText(contact.getString(Api.Fields.lastName));
                holder.companyName.setText(contact.getString(Api.Fields.companyName));
                holder.email.setText(contact.getString(Api.Fields.email));
                holder.phoneNumber.setText(contact.getString(Api.Fields.phoneNumber));

                final File file = new File(getBaseContext().getFilesDir(), "contact_pic_" + holder.id.getText().toString());
                if (file.exists() && file.length() > 0) {
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getName());
                    holder.imageView.setImageBitmap(bitmap);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return contacts.length();
        }

        public void setContacts(JSONArray contacts) {
            this.contacts = contacts;
            notifyDataSetChanged();
        }
    }

    @DebugLog
    private void fetchContacts() {
        final Map<String, String> headers = new ArrayMap<>();
        headers.put(Api.Headers.auth, SharedPrefs.getToken(getBaseContext()));

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET, Api.CONTACTS, null,
                new Response.Listener<JSONArray>() {
                    @DebugLog
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject contact = response.getJSONObject(i);
                                String id = contact.getString(Api.Fields.id);
                                assertImagePresent(id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        contactsAdapter.setContacts(response);
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        ) {
            @DebugLog
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        requestQueue.add(jsonArrayRequest);

        //CachedVolley.requestQueue.add(jsonArrayRequest);
    }

    @DebugLog
    private void assertImagePresent(String id) {
        final File file = new File(getBaseContext().getFilesDir(), "contact_pic_" + id);

        if (file.exists()) {
            return;
        } else {
            final Map<String, String> headers = new ArrayMap<>();
            headers.put(Api.Headers.auth, SharedPrefs.getToken(getBaseContext()));

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET, Api.CONTACTS + "/" + id + "/photo", null,
                    new Response.Listener<JSONObject>() {
                        @DebugLog
                        @Override
                        public void onResponse(final JSONObject response) {
                            new Thread(new Runnable() {
                                @DebugLog
                                @Override
                                public void run() {
                                    try {
                                        String content = response.getString(Api.Fields.content);
                                        byte[] contentBytes = Base64.decode(content, Base64.DEFAULT);
                                        FileOutputStream fileOutputStream = openFileOutput(file.getName(), MODE_PRIVATE);
                                        fileOutputStream.write(contentBytes);
                                        fileOutputStream.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();
                        }
                    },
                    new Response.ErrorListener() {
                        @DebugLog
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ) {
                @DebugLog
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };

            requestQueue.add(jsonObjectRequest);

            //CachedVolley.requestQueue.add(jsonArrayRequest);

        }
    }
}
