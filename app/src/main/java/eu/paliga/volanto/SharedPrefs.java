package eu.paliga.volanto;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by krzys on 07.09.2016.
 */
public class SharedPrefs {

    private static String name = "eu.paliga.volanto.SharedPrefs";
    private static String key_token = "token";

    private static String token = null;

    public static void saveToken(Context context, String token) {
        SharedPrefs.token = token;
        SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key_token, token);
        editor.commit();
    }

    public static String getToken(Context context) {
        if (SharedPrefs.token != null) {
            return SharedPrefs.token;
        } else {
            SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
            String token = sharedPreferences.getString(key_token, "");
            return token;
        }
    }
}
