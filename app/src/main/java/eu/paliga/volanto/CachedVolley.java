package eu.paliga.volanto;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

/**
 * Created by krzys on 07.09.2016.
 */
public class CachedVolley {

    public static RequestQueue requestQueue;

    private static Cache cache;

    private static Network network;

    public static void init(Context context) {
        cache = new DiskBasedCache(context.getCacheDir(), 1024*1024);
        network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);
    }
}


