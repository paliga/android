package eu.paliga.volanto;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import hugo.weaving.DebugLog;

/**
 * Created by krzys on 07.09.2016.
 */
public class ContactActivity extends AppCompatActivity {

    @Bind(R.id.id)
    TextView id;
    @Bind(R.id.firstName)
    EditText firstName;
    @Bind(R.id.lastName)
    EditText lastName;
    @Bind(R.id.companyName)
    EditText companyName;
    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.phoneNumber)
    EditText phoneNumber;

    @Bind(R.id.save)
    Button save;
    @Bind(R.id.delete)
    Button delete;
    @Bind(R.id.add_photo) Button addPhoto;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestQueue  = Volley.newRequestQueue(getBaseContext());

        setContentView(R.layout.activity_contact);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String id = intent.getStringExtra(Api.Fields.id);
        {
            if (id != null && !id.isEmpty()) {
                this.delete.setVisibility(View.VISIBLE);
                this.id.setText(id);
                this.firstName.setText(intent.getStringExtra(Api.Fields.firstName));
                this.lastName.setText(intent.getStringExtra(Api.Fields.lastName));
                this.companyName.setText(intent.getStringExtra(Api.Fields.companyName));
                this.email.setText(intent.getStringExtra(Api.Fields.email));
                this.phoneNumber.setText(intent.getStringExtra(Api.Fields.phoneNumber));
            } else {
                this.delete.setVisibility(View.GONE);
            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstName = ContactActivity.this.firstName.getText().toString();
                String lastName = ContactActivity.this.lastName.getText().toString();
                String companyName = ContactActivity.this.companyName.getText().toString();
                String email = ContactActivity.this.email.getText().toString();
                String phoneNumber = ContactActivity.this.phoneNumber.getText().toString();

                if (firstName.isEmpty() || lastName.isEmpty() || companyName.isEmpty() || email.isEmpty() || phoneNumber.isEmpty()) {
                    Toast.makeText(getBaseContext(), R.string.fields_empty, Toast.LENGTH_LONG).show();
                } else {
                    saveContact(firstName, lastName, companyName, email, phoneNumber);
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @DebugLog
            @Override
            public void onClick(View view) {
                deleteContact();
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 100);
            }
        });

    }

    @DebugLog
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                addPhoto(selectedImage);
            }
        }
    }

    @DebugLog
    private void saveContact(String firstName, String lastName, String companyName, String email, String phoneNumber) {

        int requestMethod;
        String requestUrl;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Api.Fields.firstName, firstName);
            jsonObject.put(Api.Fields.lastName, lastName);
            jsonObject.put(Api.Fields.companyName, companyName);
            jsonObject.put(Api.Fields.email, email);
            jsonObject.put(Api.Fields.phoneNumber, phoneNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (this.id.getText().toString().isEmpty()) {
            // new contact
            requestMethod = Request.Method.POST;
            requestUrl = Api.CONTACTS;
        } else {
            // existing contact
            requestMethod = Request.Method.PUT;
            requestUrl = Api.CONTACTS + "/" + this.id.getText().toString();
            try {
                jsonObject.put(Api.Fields.id, ContactActivity.this.id.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final Map<String, String> headers = new ArrayMap<>();
        headers.put(Api.Headers.auth, SharedPrefs.getToken(getBaseContext()));
        headers.put(Api.Headers.content_type, Api.Headers.application_json);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                requestMethod, requestUrl, jsonObject,
                new Response.Listener<JSONObject>() {
                    @DebugLog
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getBaseContext(), R.string.save_success, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), R.string.save_error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @DebugLog
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };


        requestQueue.add(jsonObjectRequest);

    }

    @DebugLog
    private void deleteContact() {
        final Map<String, String> headers = new ArrayMap<>();
        headers.put(Api.Headers.auth, SharedPrefs.getToken(getBaseContext()));

        String requestUrl = Api.CONTACTS + "/" + id.getText().toString();
        StringRequest stringRequest = new StringRequest(
                Request.Method.DELETE, requestUrl,
                new Response.Listener<String>() {
                    @DebugLog
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getBaseContext(), R.string.delete_success, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), R.string.delete_error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @DebugLog
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        requestQueue.add(stringRequest);
    }

    @DebugLog
    private void addPhoto(final Uri selectedImage) {
        final Map<String, String> headers = new ArrayMap<>();
        headers.put(Api.Headers.auth, SharedPrefs.getToken(getBaseContext()));
        headers.put(Api.Headers.content_type, "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");

        String requestUrl = Api.CONTACTS + "/" + id.getText().toString();
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, requestUrl,
                new Response.Listener<String>() {
                    @DebugLog
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getBaseContext(), R.string.delete_success, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @DebugLog
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getBaseContext(), R.string.delete_error, Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @DebugLog
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            @DebugLog
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    InputStream iStream = getContentResolver().openInputStream(selectedImage);
                    byte[] byteArray = ByteStreams.toByteArray(iStream);
                    return byteArray;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };

        requestQueue.add(stringRequest);
    }
}
