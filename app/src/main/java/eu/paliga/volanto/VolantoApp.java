package eu.paliga.volanto;

import android.app.Application;

/**
 * Created by krzys on 07.09.2016.
 */
public class VolantoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CachedVolley.init(this);

    }
}
